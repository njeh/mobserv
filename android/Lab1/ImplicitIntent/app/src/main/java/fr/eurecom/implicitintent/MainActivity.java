package fr.eurecom.implicitintent;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.CalendarContract;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.view.View;
import android.widget.EditText;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static fr.eurecom.implicitintent.R.id.editplace;

public class MainActivity extends AppCompatActivity {

    private EditText title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @SuppressLint("WrongViewCast")
    public void callIntent(View view) {
        Intent intent = null;
        switch (view.getId()) {
            case R.id.callBrowser:
                EditText textBrow = (EditText) findViewById(R.id.editurl);
                String url = textBrow.getText().toString().trim();
                intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://" + url));
                startActivity(intent);
                break;
            case R.id.callSomeone:
                int REQUEST_PHONE_CALL = 1;
                textBrow = (EditText) findViewById(R.id.editnum);
                String number = textBrow.getText().toString().trim();
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:"+number));
                if (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.CALL_PHONE},REQUEST_PHONE_CALL);
                    startActivity(callIntent);
                }
                else
                {
                    startActivity(callIntent);
                }
                break;
            case R.id.dial:
                textBrow = (EditText) findViewById(R.id.editnum);
                number = textBrow.getText().toString().trim();
                Intent dialIntent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" +number));
                startActivity(dialIntent);
                break;
            case R.id.showMap:
                Uri gmmIntentUri = Uri.parse("geo:0,0?q=Eurecom");
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                startActivity(mapIntent);
                break;
            case R.id.searchMap:
                textBrow = (EditText) findViewById(editplace);
                String place = textBrow.getText().toString().trim();
                gmmIntentUri = Uri.parse("geo:0,0?q="+place);
                mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                startActivity(mapIntent);
                break;
            case R.id.takePicture:
                if (ActivityCompat.checkSelfPermission(this,Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, 2);
                }
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivity(takePictureIntent);
                break;
            case R.id.showContact:
                Intent contactIntent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
                startActivity(contactIntent );
                break;
            case R.id.editContact:
                Intent intent_b = new Intent(android.content.Intent.ACTION_EDIT, Uri.parse("content://com.android.contacts/contacts/1"));
                startActivityForResult(intent_b, 1);
                break;
            case R.id.showAppChooser:
                Intent intent_x = new Intent(Intent.ACTION_VIEW);
                String njeh = "message to be sent";
                Intent chooser = Intent.createChooser(intent_x,njeh);
                if(intent_x.resolveActivity(getPackageManager())!=null){
                    startActivity(chooser);
                }
                break;
            case R.id.addToCalendar:
                Intent intent_y = new Intent(Intent.ACTION_INSERT);
                title = (EditText) findViewById(R.id.Lemon_squeezy);
                String titre = title.getText().toString().trim();
                intent_y.putExtra(CalendarContract.Events.TITLE,titre);
                intent_y.putExtra(CalendarContract.EXTRA_EVENT_ALL_DAY, true);
                intent_y.setData(CalendarContract.Events.CONTENT_URI);
                startActivity(intent_y);



        }
    }




}